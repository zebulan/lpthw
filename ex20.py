from sys import argv


def print_all(f):
    print(f.read())


def rewind(f):
    f.seek(0)


def print_a_line(line_count, f):
    print(line_count, f.readline())


script, input_file = argv

with open(input_file, 'r') as current_file:
    print("First let's print the whole file:\n")

    print_all(current_file)

    print("Now let's rewind, kind of like a tape.")

    rewind(current_file)

    print("Let's print three lines:")

    [print_a_line(line_number, current_file) for line_number in range(1, 4)]
